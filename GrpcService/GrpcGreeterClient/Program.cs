﻿using Grpc.Net.Client;
using GrpcGreeterClient;

var words = new List<string>() { "red", "yellow", "green" };

using var channel = GrpcChannel.ForAddress("https://localhost:7190");
var client = new Greeter.GreeterClient(channel);
var reply = await client.SayHelloAsync(
                  new HelloRequest { Name = "GreeterClient" });
Console.WriteLine("Greeting: " + reply.Message);
Console.WriteLine("Press any key to exit...");
Console.ReadKey();

foreach (var word in words)
{
    Request request = new Request { Word = word };
    Response response = await client.TranslateAsync(request);
    Console.WriteLine($"{response.Word} : {response.Translation}");
}

Console.ReadKey();
