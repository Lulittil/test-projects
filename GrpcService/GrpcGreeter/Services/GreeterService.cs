using Grpc.Core;
using GrpcGreeter;

namespace GrpcGreeter.Services
{
    public class GreeterService : Greeter.GreeterBase
    {
        private readonly ILogger<GreeterService> _logger;

        private readonly Dictionary<string, string> words = new() { { "red", "�������" }, { "green", "�������" }, { "blue", "�����" } };

        public GreeterService(ILogger<GreeterService> logger)
        {
            _logger = logger;
        }

        public override Task<HelloReply> SayHello(HelloRequest request, ServerCallContext context)
        {
            return Task.FromResult(new HelloReply
            {
                Message = "Hello " + request.Name
            });
        }

        public override Task<Response> Translate(Request request, ServerCallContext context)
        {
            // �������� ������������ �����
            var word = request.Word;
            Console.WriteLine($"��������� �����: {word}");
            // ���� � ������� � �������� ��� � ���������� translation
            if (!words.TryGetValue(word, out var translation))
            {
                // ���� ����� �� �������
                translation = "�� �������";
            }
            // ���������� �����
            return Task.FromResult(new Response
            {
                Word = word,
                Translation = translation
            });
        }
    }
}
