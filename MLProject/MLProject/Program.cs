﻿//Load sample data
using MLProject;

var sampleData = new SentimentModel.ModelInput()
{
    Month = @"2-Jan",
};

//Load model and predict output
var result = SentimentModel.Predict(sampleData);
